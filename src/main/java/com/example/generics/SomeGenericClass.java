package com.example.generics;

public class SomeGenericClass<E, K, T> {
    E pesel;
    K name;
    K surname;
    T password;

    public SomeGenericClass(E pesel, T password) {
        this.pesel = pesel;
        this.password = password;
    }

    public K getName() {
        return name;
    }

    public void setName(K name) {
        this.name = name;
    }

    public K getSurname() {
        return surname;
    }

    public void setSurname(K surname) {
        this.surname = surname;
    }
}
